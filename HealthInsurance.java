import java.io.FileInputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Properties;

public class HealthInsurance {
	
	Properties healthconditions = new Properties();		
	InputStream input = null;
	Integer baseInsurance = 5000;

	public HealthInsurance() {
		try {

			// input = new FileInputStream("healthConditions.properties");
			// healthconditions.load(input);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	// Base premium for anyone below the age of 18 years = Rs. 5,000
	// % increase based on age: 18-25 -> + 10% | 25-30 -> +10% | 30-35 -> +10% | 35-40 -> +10% | 40+ -> 20% increase every 5 years
	// Gender rule: Male vs female vs Other % -> Increase 2% over standard slab for Males
	// Pre-existing conditions (Hypertension | Blook pressure | Blood sugar | Overweight) -> Increase of 1% per condition
	// Habits
	// Good habits (Daily exercise) -> Reduce 3% for every good habit
	// Bad habits (Smoking | Consumption of alcohol | Drugs) -> Increase 3% for every bad habit
			
	
	public String getInsuranceDetails(HashMap personDetails) {
		String returnString = "Health Insurance Premium for";
		if("Male".equals((String)personDetails.get("Gender"))) {
			returnString = returnString + "Mr.";
		} else if("Female".equals((String)personDetails.get("Gender"))) {
			returnString = returnString + "Ms./Mrs.";
		}
		
		returnString = returnString + (String) personDetails.get("Name") + " :" + calculateHealthInsurance(personDetails);
		return returnString;
	}
		
	private float calculateHealthInsurance(HashMap personDetails) {
		float calculatedInsurance = baseInsurance;
				
		int agePercentage = 100;
		int age = Integer.parseInt((String) personDetails.get("Age"));
		// % increase based on age: 18-25 -> + 10% | 25-30 -> +10% | 30-35 -> +10% | 35-40 -> +10% | 40+ -> 20% increase every 5 years
		if(age > 18) {
			agePercentage = agePercentage + 10;
		}
		if(age > 25) {
			agePercentage = agePercentage + 10;
		}
		if(age > 30) {
			agePercentage = agePercentage + 10;
		}
		if(age > 35) {
			agePercentage = agePercentage + 10;
		}
		if(age > 40) {
			int temp = ( age + 5 ) - 40;
			temp = temp / 5;
			temp = temp * 20;
			agePercentage = agePercentage + temp;
		}
		
		calculatedInsurance = calculatedInsurance + ((baseInsurance * agePercentage) / 100);
		
		int genderInsurance = 0; 
		if("Male".equals((String)personDetails.get("Gender"))) {
			genderInsurance = (baseInsurance * 3) /100;
		}
		calculatedInsurance = calculatedInsurance + genderInsurance;
		
		
		// Hypertension: No
				// Blood pressure: No
				// Blood sugar: No
				// Overweight: Yes

				// Habits: 
				// Smoking: No
				// Alcohol: Yes
				// Daily exercise: Yes
				// Drugs: No
		
		int healthPercentage = 0;
		if("Yes".equals((String) personDetails.get("Hypertension"))) {
			healthPercentage = healthPercentage + 1;	
		}
		if("Yes".equals((String) personDetails.get("Blood pressure"))) {
			healthPercentage = healthPercentage + 1;	
		}
		if("Yes".equals((String) personDetails.get("Blood sugar"))) {
			healthPercentage = healthPercentage + 1;	
		}
		if("Yes".equals((String) personDetails.get("Overweight"))) {
			healthPercentage = healthPercentage + 1;
		}
		
		if("Yes".equals((String) personDetails.get("Smoking"))) {
			healthPercentage = healthPercentage + 3;	
		}
		if("Yes".equals((String) personDetails.get("Alcohol"))) {
			healthPercentage = healthPercentage + 3;	
		}
		if("Yes".equals((String) personDetails.get("Drugs"))) {
			healthPercentage = healthPercentage + 3;	
		}
		if("Yes".equals((String) personDetails.get("Daily exercise"))) {
			healthPercentage = healthPercentage - 3;
		}
		
		calculatedInsurance = calculatedInsurance + ((baseInsurance * healthPercentage) / 100);
				
		return calculatedInsurance;
	}
}

