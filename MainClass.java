import java.util.HashMap;
import java.util.Scanner;

public class MainClass {

	public static void main(String[] args) {
		
		// Name: Norman Gomes
		// Gender: Male
		// Age: 34 years
		// 
		// Current health:
		
		// Hypertension: No
		// Blood pressure: No
		// Blood sugar: No
		// Overweight: Yes

		// Habits: 
		// Smoking: No
		// Alcohol: Yes
		// Daily exercise: Yes
		// Drugs: No


		HashMap hdetails = new HashMap();
		
		Scanner scanner=new Scanner(System.in); 
		System.out.println("Enter your name:");
		String userInput=scanner.nextLine();
		hdetails.put("Name", userInput);
		do {
			System.out.println("Enter your Gender:Male/Female");
			userInput=scanner.nextLine();
		} while(!("Male".equals(userInput) || "Female".equals(userInput) || "Other".equals(userInput)));
		hdetails.put("Gender", userInput);
		boolean acceptable = true;
		do {
			System.out.println("Enter your Age:");
			userInput=scanner.nextLine();
			int tAge = 0;
			try {
				tAge = Integer.parseInt(userInput);
			} catch(Exception e) {
				acceptable = false;
			}
			userInput = new String("" + tAge);
		} while(!acceptable);
		hdetails.put("Age", userInput);
		System.out.println("Enter your Health Details (Comma seperated in the order):Hypertension:Yes/No,Blood pressure:Yes/No,Blood sugar:Yes/No,Overweight:Yes/No");
		userInput=scanner.nextLine();
		if(userInput.contains("Hypertension:Yes")) {
			hdetails.put("Hypertension","Yes");
		} else {
			hdetails.put("Hypertension","No");
		}
		if(userInput.contains("Blood pressure:Yes")) {
			hdetails.put("Blood pressure","Yes");
		} else {
			hdetails.put("Blood pressure","No");
		}
		if(userInput.contains("Blood sugar:Yes")) {
			hdetails.put("Blood sugar","Yes");
		} else {
			hdetails.put("Blood sugar","No");
		}
		if(userInput.contains("Overweight:Yes")) {
			hdetails.put("Overweight","Yes");
		} else {
			hdetails.put("Overweight","No");
		}
		
		
		System.out.println("Enter your Habits (Comma seperated in the order):Smoking:Yes/No,Alcohol:Yes/No,Daily exercise:Yes/No,Drugs:Yes/No");
		userInput=scanner.nextLine();
		if(userInput.contains("Smoking:Yes")) {
			hdetails.put("Smoking","Yes");
		} else {
			hdetails.put("Smoking","No");
		}
		if(userInput.contains("Alcohol:Yes")) {
			hdetails.put("Alcohol","Yes");
		} else {
			hdetails.put("Alcohol","No");
		}
		if(userInput.contains("Daily exercise:Yes")) {
			hdetails.put("Daily exercise","Yes");
		} else {
			hdetails.put("Daily exercise","No");
		}
		if(userInput.contains("Drugs:Yes")) {
			hdetails.put("Drugs","Yes");
		} else {
			hdetails.put("Drugs","No");
		}
		
		
		HealthInsurance hi = new HealthInsurance();
		System.out.println(hi.getInsuranceDetails(hdetails));
	}
}
